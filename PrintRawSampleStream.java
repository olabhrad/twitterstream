/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter4j.examples.stream;

import twitter4j.RawStreamListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.vocabulary.*;


/**
 * <p>This is a code example of Twitter4J Streaming API - sample method support.<br>
 * Usage: java twitter4j.examples.PrintRawSampleStream<br>
 * </p>
 *
 * @author Yusuke Yamamoto - yusuke at mac.com
 */
public class PrintRawSampleStream {
    /**
     * Main entry of this application.
     *
     * @param args
     */
	
	static String propertyPrefix = "http://example.twitter/";
	
	
	
	
    public static void main(String[] args) throws TwitterException {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        RawStreamListener listener = new RawStreamListener() {
            @Override
            public void onMessage(String rawJSON) {
                System.out.println(rawJSON);
                
                
                try {
					JSONObject json = new JSONObject(rawJSON);
					//System.out.println(json);
					String id = json.getString("id");
					
					String text = json.getString("text");
					String created_at = json.getString("created_at");
					String in_reply_to_use_id = json.getString("in_reply_to_user_id_str");
					String in_reply_to_status_id = json.getString("in_reply_to_status_id");
					String retweeted = json.getString("retweeted");
					
					
					//System.out.println(id);
					// create an empty model
			        Model model = ModelFactory.createDefaultModel();
			        
			        Property id_property = model.createProperty(propertyPrefix,"id");
			        Property text_property = model.createProperty(propertyPrefix,"text");
			        Property created_at_property = model.createProperty(propertyPrefix,"created_at");
			        Property in_reply_to_use_id_property = model.createProperty(propertyPrefix,"in_reply_to_use_id");
			        Property in_reply_to_status_id_property = model.createProperty(propertyPrefix,"in_reply_to_status_id");
			        Property retweeted_property = model.createProperty(propertyPrefix,"retweeted");
			        
			        
			       
			        
			        
			        
			        if (id != null) {
			        	
			        	
			        	Resource tweet = model.createResource(id)
			        			.addProperty(id_property, id)
			        			.addProperty(text_property, text)
			        			.addProperty(created_at_property, created_at)
			        			.addProperty(in_reply_to_use_id_property, in_reply_to_use_id)
			        			.addProperty(in_reply_to_status_id_property, in_reply_to_status_id)
			        			.addProperty(retweeted_property, retweeted);
			        	
			        	
			        	// now write the model in N-TRIPLES form to a file
			        	//model.write(System.out, "N-TRIPLES");
			        	//System.out.println();
			        	
			        	
			        	
			        	
			        	
			        }
			        
					
					
					
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					
				}
                
                
                
                
                
                
            }
            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        twitterStream.addListener(listener);
        twitterStream.sample();
    }
}
