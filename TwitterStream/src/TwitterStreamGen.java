import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;



/**
 * <p>This is a code example of Twitter4J Streaming API - sample method support.<br>
 * Usage: java twitter4j.examples.PrintRawSampleStream<br>
 * </p>
 *
 * @author Yusuke Yamamoto - yusuke at mac.com
 */
public class TwitterStreamGen {
    /**
     * Main entry of this application.
     *
     * @param args
     */
	
	static String propertyPrefix = "http://twitter.com#";
	static int streamSpeed = 4000;
	static String low =  propertyPrefix + "Low";
	static String medium =  propertyPrefix + "Medium";
	static String high =  propertyPrefix + "High";
	static boolean enable = false;
	static String query = "ireland";
	static TwitterStream twitterStream;
	static StatusListener listener;
	
	public static void addProperty(Resource resource, Property property, String s){
		if(!s.isEmpty()){
			resource.addProperty(property, s);
		}
	}
	

    public static void main(String[] args) throws TwitterException, InterruptedException, IOException {
    	
    	
    	
    	Window win = new Window();
    	final OWindow owin = new OWindow();
    	final List<String[]> queue = new ArrayList<String[]>();
        twitterStream = new TwitterStreamFactory().getInstance();
        listener = new StatusListener() {
            /*@Override
            public void onMessage(String rawJSON) {
                System.out.println(rawJSON);
                
                
                try {
					JSONObject json = new JSONObject(rawJSON);
					//System.out.println(json);
					String id = json.getString("id");
					
					String text = json.getString("text");
					String created_at = json.getString("created_at");
					String in_reply_to_use_id = json.getString("in_reply_to_user_id_str");
					String in_reply_to_status_id = json.getString("in_reply_to_status_id");
					String retweeted = json.getString("retweeted");
					
					
					//System.out.println(id);
					// create an empty model
			        Model model = ModelFactory.createDefaultModel();
			        
			        Property id_property = model.createProperty(propertyPrefix,"id");
			        Property text_property = model.createProperty(propertyPrefix,"text");
			        Property created_at_property = model.createProperty(propertyPrefix,"created_at");
			        Property in_reply_to_use_id_property = model.createProperty(propertyPrefix,"in_reply_to_use_id");
			        Property in_reply_to_status_id_property = model.createProperty(propertyPrefix,"in_reply_to_status_id");
			        Property retweeted_property = model.createProperty(propertyPrefix,"retweeted");
			        
			        
			       
			        
			        
			        
			        if (id != null) {
			        	
			        	
			        	Resource tweet = model.createResource(id)
			        			.addProperty(id_property, id)
			        			.addProperty(text_property, text)
			        			.addProperty(created_at_property, created_at)
			        			.addProperty(in_reply_to_use_id_property, in_reply_to_use_id)
			        			.addProperty(in_reply_to_status_id_property, in_reply_to_status_id)
			        			.addProperty(retweeted_property, retweeted);
			        	
			        	
			        	// now write the model in N-TRIPLES form to a file
			        	//model.write(System.out, "N-TRIPLES");
			        	System.out.println();
			        
			        	
			        	
			        }
			        
					
					
					
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					
				}
                
                
                
                
                
                
            }*/
            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				// TODO Auto-generated method stub
				
			}
        
        @Override
        public void onStatus(Status status) {
        	
        	
        	//if the status is a retweeted status, use the original status.
        	Status retweetedStatus = status.getRetweetedStatus();
        	if(retweetedStatus != null){
        		status = retweetedStatus;
        	};
        
    		
            User user = status.getUser();
            
            // gets Username
            String username = "\""+status.getUser().getScreenName()+"\"";
          //  username = Character.isDigit(username.charAt(0)) ? "_" + username : username;
            
            String user_id = "\""+Long.toString(user.getId())+"\""; 
            String profile_location = "\""+user.getLocation()+"\"";
            String status_id = "\""+Long.toString(status.getId())+"\""; 
            String text ="\""+status.getText()+"\"";
           // String created_at = status.getCreatedAt().toGMTString();
            
            String in_reply_to_use_id = Long.toString(status.getInReplyToUserId());
            in_reply_to_use_id = in_reply_to_use_id.equals("-1") ? "": "\""+in_reply_to_use_id+"\"";
            
            String in_reply_to_status_id = Long.toString(status.getInReplyToStatusId());
            in_reply_to_status_id = in_reply_to_status_id.equals("-1") ? "": "\""+in_reply_to_status_id+"\"";
            
            String retweetCount =  status.getRetweetCount() >= 250 ? high : 
												(status.getRetweetCount() >= 100 ? medium : low);
            
            String source = "\""+status.getSource()+"\"";
            
            String hasFollowers = user.getFollowersCount() >= 250 ? high : 
            									(user.getFollowersCount() >= 100 ? medium : low);
            
            String statusCount = user.getStatusesCount() >= 500 ? high : 
				(user.getStatusesCount() >= 250 ? medium : low);
            
          if(enable){
        	owin.update(user.getName(), Color.BLACK);
        	owin.update(" ("+user.getId()+")"+": ", Color.RED);
    		owin.update(status.getText(), Color.BLUE);
    		owin.update(" ("+status.getId()+")"+"\n\n", Color.RED);
    	
          }
            
            Model model = ModelFactory.createDefaultModel();
	        
            //Property status_id_property = model.createProperty(propertyPrefix,"statusID");
	        Property id_property = model.createProperty(propertyPrefix,"id");
	        Property username_property = model.createProperty(propertyPrefix,"username");
	        Property profile_location_property = model.createProperty(propertyPrefix,"profileLocation");
	        Property text_property = model.createProperty(propertyPrefix,"text");
	       // Property created_at_property = model.createProperty(propertyPrefix,"createdAt");
	        Property in_reply_to_use_id_property = model.createProperty(propertyPrefix,"inReplyToUser");
	        Property in_reply_to_status_id_property = model.createProperty(propertyPrefix,"inReplyToStatus");
		    Property retweet_count_property = model.createProperty(propertyPrefix,"retweetCount");
	        Property source_property = model.createProperty(propertyPrefix,"source");
	        
	        
	        Property user_id_property = model.createProperty(propertyPrefix,"userID");
	        Property has_followers_property = model.createProperty(propertyPrefix,"hasFollowers");
	        Property status_count_property = model.createProperty(propertyPrefix,"statusCount");
	        
	        if (status_id != null) {
	        	
	        	
	        	Resource tweetID = model.createResource(status_id);
	        			//addProperty(tweetID,id_property, id);
	        			addProperty(tweetID,user_id_property, user_id);
	        			addProperty(tweetID,username_property, username);
	        			addProperty(tweetID,profile_location_property,profile_location);
	        			addProperty(tweetID,text_property, text);
	        			//addProperty(created_at_property, created_at)
	        			addProperty(tweetID,in_reply_to_use_id_property, in_reply_to_use_id);
	        			addProperty(tweetID,in_reply_to_status_id_property, in_reply_to_status_id);
	        			addProperty(tweetID,retweet_count_property, retweetCount);
	        			addProperty(tweetID,source_property, source);
	        	
	        	Resource userID = model.createResource(user_id);
	        		addProperty(userID,has_followers_property, hasFollowers);
	        		addProperty(userID,status_count_property, statusCount);
	        	// now write the model in N-TRIPLES form to a file
	        	//model.write(System.out, "N-TRIPLES");
	        	
	        	
	        	StmtIterator it = model.listStatements();
	        	
	        	while(it.hasNext()){
	        		Statement statement = it.next();
	        		String [] triparray = new String[4];
	        		triparray[0] = statement.getSubject().toString();
	        		triparray[1] = statement.getPredicate().toString();
                    triparray[2] = statement.getObject().toString();
                    triparray[3] = String.valueOf(System.currentTimeMillis());
                    queue.add(triparray);
	        	}


	        }

        }
        };
        
        FilterQuery fq = new FilterQuery();
        //count - Indicates the number of previous statuses to stream before transitioning to the live stream.
        //follow - Specifies the users, by ID, to receive public tweets from.
        //track - Specifies keywords to track. (includes hashtags)
        //locations - Specifies the locations to track. 2D array
        //language - Specifies the tweets language of the stream
        String keywords[] = {query};
        String language[] = {"en"};

        
        fq.track(keywords);
        fq.language(language);

        twitterStream.addListener(listener);
        twitterStream.filter(fq);  
    
    
    
    ServerSocket serversocket = new ServerSocket(5004);
    Socket accept = serversocket.accept();
    
    OutputStream outputStream = accept.getOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(outputStream);
   
    
    while (true) {


        Thread.sleep(streamSpeed);//Stream Speed       
        
        if(enable){
	        for (int j = 0; j < queue.size(); j++) {
	            //System.out.println(queue.get(j));
	            oos.reset();
	            Object o = queue.get(j);
	            oos.writeObject(o);
	        }
        }
        queue.clear();
    }
 
    }
    
    public static class Window 
    {
    	
    	private static final int WIDTH = 100;
    	private static final int HEIGHT = 100;
    	
    	private JButton OK;
    	public JTextField input;
    	
    	//Button handlers:
    	private InputHandler OKHandler;

    	
    	public Window()
    	{
    		
    		input = new JTextField(1);
    		
    		//SPecify handlers for each button and add (register) ActionListeners to each button.
    		OK = new JButton("OK");
    		InputHandler OKHandler = new InputHandler();
    		OK.addActionListener(OKHandler);

    		
    		JFrame win = new JFrame();
    		win.setTitle("Input");
    		Container all = win.getContentPane();
    		all.setLayout(new GridLayout(2, 1));
    		
    		Container pane = new Container();
    		Container pane2 = new Container();
    		pane.setLayout(new GridLayout(1, 1));
    		pane2.add(OK);
    		pane2.setLayout(new GridLayout(1, 1));
    	
    		
    		//Add things to the pane in the order want them to appear (left to right, top to bottom)
    		pane.add(input);
    		
    		all.add(pane);
    		all.add(pane2);
    		
    		win.setSize(WIDTH, HEIGHT);
    		win.setVisible(true);
    		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	}
 
    	
    	public class InputHandler implements ActionListener
    	{	
    		public InputHandler(){
    		}
    		public void actionPerformed(ActionEvent e)
    	
    		{	
    				enable = !enable;
    				String s = input.getText();
    				if(!s.isEmpty()){
    					query = s;
	    				twitterStream = new TwitterStreamFactory().getInstance();
	    				FilterQuery fq = new FilterQuery();
	    				String keywords[] = {query};
	    		        String language[] = {"en"};

	    		        
	    		        fq.track(keywords);
	    		        fq.language(language);
	
	    		        twitterStream.addListener(listener);
	    		        twitterStream.filter(fq);  
    				}
    		
    				input.setText("");
    			
    		}
    	}
    	
    	
    }
	
    public static class OWindow 
    {
    	
    	
    	JTextPane textArea;

		Color[] Colors = {Color.BLACK,Color.BLUE,Color.GREEN,Color.MAGENTA,
				Color.ORANGE,Color.LIGHT_GRAY,Color.PINK,Color.RED};
		int colorI = 0;
		int runningCount = 1;
    	
    	public OWindow(){
    		
    	    //FRAME
    		JFrame frame = new JFrame ("TweetStream");
    		frame.setSize(600,500);
    		frame.setResizable(false);

    		//TEXT AREA
    		textArea = new JTextPane();
    		textArea.setSize(500,400);    
    		    textArea.setVisible(true);

    		    JScrollPane scroll = new JScrollPane (textArea);
    		    scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    		          scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    		    frame.add(scroll);
    		    frame.setVisible(true);
    		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	}
 
    	
    	public void update(String msg, Color c)
	    {
	        if(colorI==8){
	        	colorI=0;
	        }
	        
	        StyleContext sc = StyleContext.getDefaultStyleContext();
	        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);
	        

	        
	        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
	        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

	        int len = textArea.getDocument().getLength();
	        textArea.setCaretPosition(len);
	        textArea.setCharacterAttributes(aset, false);
	        textArea.replaceSelection(msg);
	        runningCount++;
	    }
    	
    	public void newIt(){
    		if(colorI==8){
	        	colorI=0;
	        }
	        
    		StyleContext sc = StyleContext.getDefaultStyleContext();
	        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Colors[colorI]);
    		colorI++;
        	int len = textArea.getDocument().getLength();
	        textArea.setCaretPosition(len);
	        textArea.setCharacterAttributes(aset, false);
	        textArea.replaceSelection("\n");
    	}
    	
    }    
}
